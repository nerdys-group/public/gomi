# FUN Random notes and Snippets

## Creating a AES-256 base-64 key using python.

```
python -c 'import base64; import os; print(base64.encodestring(os.urandom(32)))'
```

## In GCP, Use the following to update or add a service account to GCP compute instance
(First export the service accounts, credential key, rename to something less confusing, upload to the instance and run the following on the instance.)

```
gcloud auth activate-service-account --key-file credentials.json 
```

## Easy MySQL import from CSV
Create your table first!

```
mysqlimport -h $DB_ADDR --ignore-lines=1 --fields-terminated-by=, resources --verbose --local -u root -p billing.csv
```

## Setting projects in gcloud cli

```
gcloud config set compute/region REGION
```
```
gcloud config set compute/zone ZONE`
```

## Setting up VPN's in GCP

### Create Target VPN Gateways

```
gcloud compute target-vpn-gateways \
create vpn-1 \
--network vpn-network-1  \
--region us-east1
```

```
gcloud compute target-vpn-gateways \
create vpn-2 \
--network vpn-network-2  \
--region europe-west1
```

### Create Static IP's (in each VPC

```
gcloud compute addresses create --region us-east1 vpn-1-static-ip
```

### Create Forwarding Rules

```
gcloud compute \
forwarding-rules create vpn-1-esp \
--region us-east1  \
--ip-protocol ESP  \
--address $STATIC_IP_VPN_1 \
--target-vpn-gateway vpn-1
```

```
gcloud compute \
forwarding-rules create vpn-2-esp \
--region europe-west1  \
--ip-protocol ESP  \
--address $STATIC_IP_VPN_2 \
--target-vpn-gateway vpn-2
```

### Create for UDP500

```
gcloud compute \
forwarding-rules create vpn-1-udp500  \
--region us-east1 \
--ip-protocol UDP \
--ports 500 \
--address $STATIC_IP_VPN_1 \
--target-vpn-gateway vpn-1
```

```
gcloud compute \
forwarding-rules create vpn-2-udp500  \
--region europe-west1 \
--ip-protocol UDP \
--ports 500 \
--address $STATIC_IP_VPN_2 \
--target-vpn-gateway vpn-2
```

### UDP4500

```
gcloud compute \
forwarding-rules create vpn-1-udp4500  \
--region us-east1 \
--ip-protocol UDP --ports 4500 \
--address $STATIC_IP_VPN_1 \
--target-vpn-gateway vpn-1
```

```
gcloud compute \

```forwarding-rules create vpn-2-udp4500  \
--region europe-west1 \
--ip-protocol UDP --ports 4500 \
--address $STATIC_IP_VPN_2 \
--target-vpn-gateway vpn-2
```

## List Target Gateways

```
gcloud compute target-vpn-gateways list
```

### Create Tunnels

```
gcloud compute \
vpn-tunnels create tunnel1to2  \
--peer-address $STATIC_IP_VPN_2 \
--region us-east1 \
--ike-version 2 \
--shared-secret gcprocks \
--target-vpn-gateway vpn-1 \
--local-traffic-selector 0.0.0.0/0 \
--remote-traffic-selector 0.0.0.0/0
```

```
gcloud compute \
vpn-tunnels create tunnel2to1 \
--peer-address $STATIC_IP_VPN_1 \
--region europe-west1 \
--ike-version 2 \
--shared-secret gcprocks \
--target-vpn-gateway vpn-2 \
--local-traffic-selector 0.0.0.0/0 \
--remote-traffic-selector 0.0.0.0/0
```

```
gcloud compute vpn-tunnels list
```

## Create route

```
gcloud compute  \
routes create route1to2  \
--network vpn-network-1 \
--next-hop-vpn-tunnel tunnel1to2 \
--next-hop-vpn-tunnel-region us-east1 \
--destination-range 10.1.3.0/24
```

```
gcloud compute  \
routes create route2to1 network vpn-network-2 \
--next-hop-vpn-tunnel tunnel2to1 \
--next-hop-vpn-tunnel-region europe-west1 \
--destination-range 10.5.4.0/24
```

## Testing load balancing/failures.

In GCP you can test a failure scenario by tagging the instance with:

```
failure_zone
```

This will remove that vm from the loadbalancer/Autoscaler. How fucking neat is that.

## Some Guidance for Managed Instance Groups with Load Balancing

1. Do not put a VM in more than one instance group
2. Do not delete an instance group if its being used by a backend
3. Avoid adding the same instance group to two different backands
4. All instances in a managed or unmanaged instance group must be in the same VPC
5. If using a MIG with autoscaling, don't use maxRate balancing mode in the backend service.
6. Don't make an autoscaled MIG the target of two different load balancers
7. Max size of a MIG should be <= size of the subnet.

## Some notes on Meta data.

You can query Meta data from Console, Cloudshell or API

From the VM you can query hitting the URL's:

* http://metadata.google.internal/computeMetadata/v1/
* http://metadata/computeMetadata/v1
* http://<ip-address>/computeMetadata/v1/

From from the gcloud command

Project: gcloud compute project-info describe
Instance: gcloud compute instances describe <INSTANCE>

### Use curl to query metadata changes

```
curl "http://metadata/computeMetadata/v1/instance/tags?wait_for_change=true" -H "Metadata-Flavor: Google"
```

### Handling maintenance events

/scheduling directory 
maintenance-event attribute

### NOTE

It is up to us to monitor metadata tags which will allow us to respond to maintenance events.

## Startup and Shutdown scripts

You can point to a URL for a script

```
gcloud compute instances create <instance> --metadata startup-script-url=<url>
```
Storing the script in a bucket is a good way to go.

You can also upload directly with the key startup-script

Logs for the script can be found on the instance.. In Debian it's /var/log/daemon.log

## Setting up GKE

### Cluster

```
gcloud container clusters create networklb --num-nodes <number of nodes>
```

### Deploying containers

```
kubectl run nginx --image=nginx --replicas=3
```

Check on the pod status

```
kubectl get pods -owide
```

exposing through the network

```
kubectl expose deployment nginx --port=80 --target-port=80 --type=LoadBalancer
```

exposing on random port

```
kubectl expose deployment nginx --target-port=80 --type=NodePort
```

More info [here.](https://kubernetes.io/docs/concepts/services-networking/service/)

find your exposed address

```
kubectl get service nginx
```

### ingress rules yaml setup

Below is an example ingress setup for kubernetes
```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: example-ingress
spec:
  backend:
    serviceName: default-handler
    servicePort: 80
  rules:
  - host: my.app.com
    http:
      paths:
      - path: /tomcat
        backend:
          serviceName: tomcat
          servicePort: 8080
      - path: /nginx
        backend:
          serviceName: nginx
          servicePort: 80
```

The create the rules by running this

```
kubectl create -f basic-ingress.yaml
```

and describe the rules like so:

```
kubectl describe ingress basic-ingress
```

To list the address:

```
kubectl get ingress basic-ingress
```

### Deleting services and deployments and clusters

```
kubectl delete service nginx
```

```
kubectl delete deployment nginx
```

```
gcloud container clusters delete networklb 
```




 


