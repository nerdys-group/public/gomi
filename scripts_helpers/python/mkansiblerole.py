#!/usr/bin/env python
import os
import argparse

# Stupid simple script that creates an ansible role directory and it's structure.

parser = argparse.ArgumentParser()

parser.add_argument("role_name", help="name of the role you want to create", type=str)
args = parser.parse_args()

print(f"creating role '{args.role_name}'")

role_root_path = os.path.join("./", args.role_name)

os.mkdir(role_root_path)

#making all the folders

tasks_dir = os.path.join(role_root_path, "tasks")
handlers_dir = os.path.join(role_root_path, "handlers")
library_dir = os.path.join(role_root_path, "library")
files_dir = os.path.join(role_root_path, "files")
vars_dir = os.path.join(role_root_path, "vars")
defaults_dir = os.path.join(role_root_path, "defaults")
templates_dir = os.path.join(role_root_path, "templates")
meta_dir = os.path.join(role_root_path, "meta")

os.mkdir(tasks_dir)
os.mkdir(handlers_dir)
os.mkdir(library_dir)
os.mkdir(files_dir)
os.mkdir(vars_dir)
os.mkdir(defaults_dir)
os.mkdir(templates_dir)
os.mkdir(meta_dir)

#create a yaml's and files for gitkeep.

filenames = []

filenames.append(os.path.join(tasks_dir, "main.yml"))
filenames.append(os.path.join(handlers_dir, "main.yml"))
filenames.append(os.path.join(library_dir, ".gitkeep"))
filenames.append(os.path.join(files_dir, ".gitkeep"))
filenames.append(os.path.join(vars_dir, "main.yml"))
filenames.append(os.path.join(defaults_dir, "main.yml"))
filenames.append(os.path.join(templates_dir, ".gitkeep"))
filenames.append(os.path.join(meta_dir, "main.yml"))

for filename in filenames:
    open(filename, 'a')



