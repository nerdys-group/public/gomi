# Davin's Tmux setup.

# Fix terminal options
set-option -sa terminal-overrides ",xterm*:Tc"

# make tmux work with kitty
set -g default-terminal "kitty"

# Set my prefix keys to control space
unbind C-b
set -g prefix C-Space
bind C-Space send-prefix

# Set mouse support
set -g mouse on

# set motion in copy mode and yanking
set-window-option -g mode-keys vi

bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi C-v send-keys -X rectangle-toggle
bind-key -T copy-mode-vi y send-keys -X copy-selection-and-cancel

# Set indexing of windows from 1
set -g base-index 1
set -g pane-base-index 1
set-window-option -g pane-base-index 1
set-option -g renumber-windows on

# setting split panes to stay in cwd of pane split from
bind '"' split-window -v -c "#{pane_current_path}"
bind '%' split-window -h -c "#{pane_current_path}"

# setting new window to stay in cwd of the window it was created from
bind c new-window -c "#{pane_current_path}"


# Plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'christoomey/vim-tmux-navigator'
set -g @plugin 'dreamsofcode-io/catppuccin-tmux'
set -g @plugin 'tmux-plugins/tmux-yank'

run '~/.tmux/plugins/tpm/tpm'
