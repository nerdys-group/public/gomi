# gomi

Place for junk and dot files ... gomi is Japanese for junk

## DotFiles. Setting up the profiles

### Vim

We have a basic install and some choice plugins that need to be install.

#### Dependancies

 * cmake
 * node
 * npm
 * python3-dev
 * python2-dev (python-dev in some distros) 
 * golang
 * rust

#### Vundle

```bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

#### Youcompleteme .. is a little special

It requires some extra steps and compilation.

From the you complete me folder (~/.vim/bundle/youcompletme/)

```bash
git submodule update --init --recursive
```

Then run the install python script which will do some compilation. (may or may not need sudo)

```bash
sudo ./install.py --gocode-completer --racer-completer --tern-completer
```

### ZSH


### AwesomeWM


### Xresources stuff 
